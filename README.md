# SYNIT: A Reactive Operating System

 - Homepage: <https://synit.org/>
 - See also: <https://syndicate-lang.org/projects/2021/system-layer/>

Synit is an experiment in applying **pervasive reactivity** and **object capabilities** to the
[System Layer](https://synit.org/book/glossary.html#system-layer) of an operating system for
personal computers, including laptops, desktops, and mobile phones. Its
[architecture](https://synit.org/book/architecture.html) follows the principles of the
[Syndicated Actor Model](https://synit.org/book/glossary.html#syndicated-actor-model).

Synit builds upon the Linux kernel, but replaces many pieces of
familiar Linux software, including `systemd`, `NetworkManager`,
`D-Bus`, and so on. It makes use of many concepts that will be
familiar to Linux users, but also incorporates many ideas drawn from
programming languages and operating systems not closely connected with
Linux's Unix heritage.

## Quickstart

If you have a mobile phone or computer capable of running
[PostmarketOS](https://postmarketos.org/), then you can [install](https://synit.org/book/install.html)
the software to try it out. You can also run Synit inside a virtual machine.

See the [installation instructions](https://synit.org/book/install.html) for a list of supported
devices.

## Documentation

See [the Synit Manual](https://synit.org/book/).

## Repository contents

 - [`synit-pid1/`](synit-pid1/) is a small, simple Rust program implementing a minimal PID1 for
   the Synit system.

 - [`protocols/schemas/`](protocols/schemas/) contains schema definitions for the protocols
   used among the components of the system.

 - [`packaging/`](packaging/) contains Alpine (PostmarketOS) packaging scripts and supporting
   files for Synit components. The packages themselves live in
   [`packaging/packages/`](packaging/packages/). A particularly interesting package is
   [`synit-config`](packaging/packages/synit-config/).

 - [`scripts/`](scripts/) contains support scripts for uploading and installing Synit packages
   on a target device.
