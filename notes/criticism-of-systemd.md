---
title: 'Criticism of systemd'
---

https://skarnet.org/software/systemd.html

http://ewontfix.com/14/
 - keep supervision and system state management out of PID 1
 - there's a sample PID 1 implementation there

"systemd, 10 years later: a historical and technical retrospective"
https://blog.darknedgy.net/technology/2020/05/02/0/index.html#decline
