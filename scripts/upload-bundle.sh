#!/bin/sh

# Upload packages and scripts for converting a plain PostmarketOS system into a Synit system

set -eu

HOST=${1:-pm-qemu}
SUDOPASS=${SUDOPASS:-user}

ssh ${HOST} "echo ${SUDOPASS} | sudo -S apk add rsync"
rsync -avu --delete \
      ./transmogrify.sh \
      ../packaging/synit-apk-key.pub \
      ../packaging/target/packages \
      ${HOST}:.
