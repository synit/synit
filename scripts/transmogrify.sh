#!/bin/sh

# Transmogrifies a running, plain PostmarketOS system into a Synit system

set -eu

SUDOPASS=${SUDOPASS:-user}
REBOOT_AFTER_TRANSMOGRIFY=${REBOOT_AFTER_TRANSMOGRIFY:-}

if [ -z "${http_proxy:-}" ]
then
    if ip a | grep -q -F 10.0.2
    then
        http_proxy=http://10.0.2.2:3128/
    else
        http_proxy=
    fi
fi

if [ $(id -u) -ne 0 ]
then
    echo ${SUDOPASS} | \
        sudo -S \
             "http_proxy=${http_proxy}" \
             "REBOOT_AFTER_TRANSMOGRIFY=${REBOOT_AFTER_TRANSMOGRIFY}" \
             "$0" "$@"
    exit $?
fi
echo

cp synit-apk-key.pub /etc/apk/keys/.
[ -f /usr/share/deviceinfo/deviceinfo ] && . /usr/share/deviceinfo/deviceinfo
[ -f /etc/deviceinfo ] && . /etc/deviceinfo
ls packages/$deviceinfo_arch/

echo "Will use http_proxy=${http_proxy}"
apk add -X `pwd`/packages \
     preserves-tools \
     squeak-cog-vm \
     syndicate-sh \
     synit-config \
     \
     alsa-utils \
     qmi-utils \
     mpg123 \
     \
     docker

if [ -n "$REBOOT_AFTER_TRANSMOGRIFY" ]
then
    reboot
fi
