#!/bin/sh

mount -t devtmpfs none /dev

mkdir /dev/pts
mount -t devpts none /dev/pts

mkdir /dev/shm
mount -t tmpfs -o nodev,nosuid,exec none /dev/shm
# ^ must be mounted "exec" to allow JIT for Squeak

mount -t proc none /proc

mount -t sysfs none /sys

mount -t tmpfs none /run

# Docker wants this
mount -t cgroup2 none /sys/fs/cgroup

mount -o rw,remount /

mkdir -p /etc/syndicate/user-settings
mkdir -p /run/etc/syndicate/core
mkdir -p /usr/local/etc/syndicate/core
mkdir -p /run/etc/syndicate/services
mkdir -p /usr/local/etc/syndicate/services

# This is a grody hack. We are replacing the infrastructure that sets up RNDIS, and in
# particular we no longer want to run the DHCP server that PostmarketOS runs by default on
# usb0. Instead we will be a DHCP client. (This was just easiest to set up.)
killall unudhcpd || true
# We also don't want the splash screen.
killall pbsplash || true

# This tells Rust programs built with jemallocator to be very aggressive about keeping their
# heaps small. Synit currently targets small machines. Without this, I have seen the system
# syndicate-server take around 300MB of heap when doing not particularly much; with this, it
# takes about 15MB in the same state. There is a performance penalty on being so aggressive
# about heap size, but it's more important to stay small in this circumstance right now.
export _RJEM_MALLOC_CONF="narenas:1,tcache:false,dirty_decay_ms:0,muzzy_decay_ms:0"

exec /sbin/synit-pid1
