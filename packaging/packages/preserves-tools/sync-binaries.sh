#!/bin/sh
cd "$(dirname "$0")"

set -e

build() {
    make -C ~/src/preserves/implementations/rust $1-binary-release 2>&1 | tee buildlog.$1
    cp -p ~/src/preserves/implementations/rust/target/$1-*/release/preserves-tool preserves-tool.$1
}

build x86_64 &
build armv7 &
build aarch64 &
wait
